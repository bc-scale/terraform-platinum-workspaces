locals {
  hcloud_token_workspaces = [
    tfe_workspace.platinum,
  ]
  cloudflare_token_workspaces = [
    tfe_workspace.platinum,
  ]
}

resource "tfe_variable" "hcloud_token" {
  count        = length(local.hcloud_token_workspaces)
  key          = "hcloud_token"
  value        = var.hcloud_token
  category     = "terraform"
  sensitive    = true
  workspace_id = local.hcloud_token_workspaces[count.index].id
}

resource "tfe_variable" "cloudflare_token" {
  count        = length(local.cloudflare_token_workspaces)
  key          = "cloudflare_token"
  value        = var.cloudflare_token
  category     = "terraform"
  sensitive    = true
  workspace_id = local.cloudflare_token_workspaces[count.index].id
}
