# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/gitlabhq/gitlab" {
  version     = "3.7.0"
  constraints = "~> 3.7.0"
  hashes = [
    "h1:Xe8KHFOG3zA61RjvDtVgWKCPeT4l1++XEGVJyNEeeM4=",
    "zh:16addba6bda82e6689d21049392d296ce276cb93cbc5bcf3ad21f7d39cd820fd",
    "zh:1e9dd3db81a38d3112ddb24cec5461d919e7770a431f46ac390469e448716fd4",
    "zh:252c08d473d938c2da2711838db2166ecda2a117f64a10d279e935546ef7d991",
    "zh:2e0c83da0ba44e0521cb477dd60ea7c08b9edbc44b607e5ddb963becb26970a5",
    "zh:396223391782f1f809a60f045bfdcde41820d0d6119912718d86fc153fc28969",
    "zh:3a6b3c0901b81bc451d1ead2a2f26845d5db6b6253758c1f0aa0bad53fb6b4bd",
    "zh:51010e8f1d05f4979f0e10cf0e3b56cec13c731d99f373dda9fd9561ddb2826b",
    "zh:53ef55edf7698cbb4562265a6ab9e261716f62a82590e5fb6ad4f7f7458bdc5c",
    "zh:6c2db10e6226cc748e6dd5c1cbc257fda30cd58a175a32fc95a8ccd5cebdd3e7",
    "zh:91627f5af7e8315479a6c45cb1ae5db3c0a91a18018383cd409f3cfa04408aed",
    "zh:b5217a81cfc58334278831eacb2865bd8fc025b0cb1c576e9da9c4dc3a187ef5",
    "zh:c70afea4324518b099d23abc189dff22e6706ca0936de39eca01851e2550af7e",
    "zh:e62c212169ef9aad3b01f721db03b7e08d7d4acbbac67a713f06239a3a931834",
  ]
}

provider "registry.terraform.io/hashicorp/tfe" {
  version     = "0.26.1"
  constraints = "~> 0.26.0"
  hashes = [
    "h1:EI3qNBH48uHsbsFQsz7b0EjtJsMEbTP+6L2AHKE6/pc=",
    "zh:0819d398848bd29384364c71a2722014920a5d43a2758574472923a27303b2d2",
    "zh:101db61eac908bd0b35a68cdf54ef647c7e25d9837e4bff8b667d65452bad217",
    "zh:35c9aee4615ba355b554ee9545d805f34d9d2288eea84205f72a9b0215657286",
    "zh:58870295ff4dd39881ad962a610b50205aaa4bc546ee63871659ee76fccec239",
    "zh:92d3e95cd136c2eb9e6a6f644e9ca0cd7689ba84013a3354d0dd6fe391bca5aa",
    "zh:9cf2f7491f8ca9c41d762114aa41d545a9bfff822a8a1ba1279feb8987c31e1d",
    "zh:a3bfded94d10a8271c7d583190f2e5e5af527fa1273348dfc49287b1d90ef7eb",
    "zh:b1f96239fa4caca4f3d853835da49f689b962e0be6b2aa60e3bed53f940c19fa",
    "zh:db079ec70f87481c2e45fa59c48fe51b64f1ddc57712b2def03f4d487882fa18",
    "zh:dd582833f346424d242e1f3401b4e2210cef0cde8ea8f1906b375e1dd493f60f",
    "zh:eb0182431c51e958e368c002a9789310dcdecf38de16d24d6c04820284791d5e",
  ]
}
